package serveur

import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.random.Random

class Message
{
    lateinit var message:ByteArray;
    lateinit var fileExtension : String;
    lateinit var messageCoded : IntArray;
    lateinit var messageDecoded : ByteArray;
    val lengthSeed=17;
    val seed=IntArray(lengthSeed); //
    private lateinit var mask:IntArray;

    //constructeur
    init {initSeed();}

    //initialise le tableau de graine par des valeurs binaires aleatoire
    private fun initSeed()
    {
        for(i in 0..16)
        {
            val r= Random.nextInt(0,2);
            this.seed[i]=r;
        }
    }

    fun uploadFile(fileName : String):Boolean
    {
        val file= File(fileName);

        if(file.exists())
        {
            this.fileExtension = file.extension;
            this.message = Files.readAllBytes(Paths.get(fileName));
            return true;
        }
        else
        {
            return false;
        }
    }

  /*  fun uploadMessage(msg:String)
    {
        this.message= msg;
    }
*/
    private fun lfsr(n:Int)= this.mask[n-5] xor this.mask[n-6] xor this.mask[n-15] xor this.mask[n-16]

    /**
     * rempli le code pour le codage
     */
    fun generateMask()
    {
        val lengthMask = this.message.size; //la longueur du masque total doit etre egal à la longueur du message à coder
        this.mask=IntArray(lengthMask);

        for(i in this.mask.indices)
            if(i<lengthSeed)
                this.mask[i]=seed[i]
            else
                this.mask[i]=lfsr(i)

      //  println(this.mask.contentToString()+"\n")
    }

    fun codingMessage() {
        generateMask();
        messageCoded = IntArray(this.message.size)
        messageDecoded = ByteArray(this.messageCoded.size);
        for( i in this.mask.indices)
        {
            messageCoded[i]= this.message[i].toInt().xor(this.mask[i]);
            messageDecoded[i]=messageCoded[i].toByte();
        };
    }

    fun decodingMessage()
    {

        for(i in this.mask.indices)
        {
            val asciiMsg=messageCoded[i].xor(this.mask[i]);
            messageDecoded[i]= (messageCoded[i].xor(this.mask[i]).toByte());
        };

    }

    fun sendMessage(fileName: String) : IntArray
    {
        //on importe le message
        uploadFile(fileName);

        generateMask();
        codingMessage();
        val msgCoded = this.messageCoded;

        return msgCoded
    }


    fun saveJournal():String
    {
       // decodingMessage();
        //on enregistre le message decodé dans le bon format
        val txt=downloadFile();

        val time = LocalDateTime.now();
        var format = DateTimeFormatter.ofPattern("dd-MM-YYYY_HHmmss");
        var formatedTime = time.format(format);

        val fileName = "detail_traitement_$formatedTime.txt";
        val outputFile= File(fileName);

        format = DateTimeFormatter.ofPattern("dd-MM-YYYY HH:mm:ss");
        formatedTime = time.format(format);
        var journal="message reçu a : $formatedTime\n\n";
        journal += "longueur message : ${mask.size}\n";
        journal += "format : ${this.fileExtension}\n\n\n";
        journal += "-------Message codé reçu -------\n\n";
        journal += messageCoded.contentToString()+"\n\n\n";
        journal += "--------- LFSR DETAILS ---------\n\n";
        journal += "graine : ${this.seed.contentToString()}\n";
        journal += "masque : ${this.mask.contentToString()}\n\n\n";
        journal += "========= MESSAGE DECODE ==========\n\n";
        journal += String(this.messageDecoded);
        if(outputFile.createNewFile())
        {
            outputFile.writeText(journal);
            return "Le decodage du message est effectué avec succès, vous trouverez les details du traitement dans le fichier ${fileName}";
        }
        else
            return "Erreur de traitement";
    }

    fun downloadFile():String
    {
        val time = LocalDateTime.now();
        val format = DateTimeFormatter.ofPattern("dd-MM-YYYY_HHmmss");
        val formatedTime = time.format(format);

        val fileName = "decodedMessage_${formatedTime}."+this.fileExtension;

        Files.write(Paths.get(fileName), messageDecoded);

        return "Le message a été enregistré sous le nom de ${fileName}";
        //creation de l'objet file
       // val outputFile = File(fileName);

       /* if(outputFile.createNewFile())
        {
            outputFile.writeText(messageDecoded);
            println("Fichier sauvegardé avec succes sous le nom "+ fileName);
        }
        else
            println("Une erreur est survenu lors de l'enregistrement du fichier");*/
    }
}

fun main(args: Array<String>)
{
    val msg= Message();

    val msgCoded = msg.sendMessage("message.txt");
    val res=msg.saveJournal();
    //msg.uploadFile("message.m4a");

    /*msg.uploadMessage("ceci est un message test, mais cette fois ci, on essaies de le rendre encore plus longue");
*/
   /* println("graine : "+msg.seed.contentToString() +"\n")
    println(msg.message.toString())
    msg.generateMask();

    val msgCode = msg.codingMessage();
    println("you got a coded message :"+msgCode.contentToString()+"\n")

    println("message decoding...");
    val msgDecoded= msg.decodingMessage(msgCode);
    println(msgDecoded)*/
}